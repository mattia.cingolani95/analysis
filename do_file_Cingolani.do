***With this analysis I try to undestrand the propensity of Italian students to enter the world of work.
***In particular the goal is to understand what are statistically significant variables that influence 
***the entry into the working world by an Italian student. The data relating to Personal Variables come 
***from the MICRODATA dataset for public use by ISTAT (Italian National Statistical Institute)
***"professional integration of graduates" of the year 2015. The Regional Context Variables come from the
***official ISTAT website in the dedicated sections.
***For this analysis I used a Multilevel Regression that is useful to analyse data with hierarchical structure,
***because offers a compromise between aggregate data analysis and microlevel analysis in a non-geographic context.
***Multilevel models can appropriately handle variables measured at different levels it becomes possible 
***to simultaneously study micro and macro-level determinants of several dynamics. 
***Units in the same area tend to be more similar to each other than units in different area. As a result
***of this within-area correlation, standard errors can be estimated with a downward bias and, hence, 
***inferences are not corrects conducing to spurious significant results.A simple solution would be to compute
***robust standard errors. However, when the multilevel structure is not only a mere nuisance factor but 
***instead an interesting dimension of the analysis, multilevel models are more appealing because they can
***help answering some research questions that otherwise would be impossible or impractical to answer.

 

import delimited



*** A)I CLEAN THE DATASET KEEPING USEFUL VARIABLES AND KEEP ONLY LAVORATORI DIPENDENTI 
keep reg_uni_micro tiplau_micro sesso eta_laurea_mfr l1_17 l1_18_mfr l1_20 l1_22 l2_1 l2_55 gruppo_micro 


***************************************************************************************************************************

*** B) DESCRIPTION OF PERSONAL VARIABLES dataset\InsProLau_Microdati_Anno_2015
label variable reg_uni_micro University_region
label variable tiplau_micro Educational_qualification
label variable sesso Sex
label variable eta_laurea_mfr Age
label variable l1_17 Erasmus_participation
label variable l1_18_mfr Foreign_State_Erasmus
label variable l1_20 University_in_progress
label variable l1_22 Graduation_grade
label variable l2_1 Looking_for_job
label variable l2_55 Level_of_satisfaction 
label variable gruppo_micro Degree_type

*** - INDIPENDENT VARIABLES

desc reg_uni_micro tiplau_micro gruppo_micro sesso eta_laurea_mfr l1_17 l1_18_mfr l1_20 l1_22 l2_55

*** - DIPENDENT VARIABLE

desc l2_1

***************************************************************************************************************************

*** C) RECODING DUMMY VARIABLES

*Rewrite sex, Eramus_participation, University_in_progress and Looking_for_job into dummy variables (0,1)
*instead of (1,2) to simplifyng the model   
recode sesso (1 = 0) (2 = 1)
recode l1_17 (1 = 0) (2 = 1)
recode l1_20 (1 = 0) (2 = 1)
recode l2_1 (1 = 0) (2 = 1)

*Rewrite Educational_qualification so as to be divided into "Combined Degree", "Master's Degree" and "Bachelor's Degree"
destring tiplau_micro, replace
gen Combined_degree = (tiplau_micro==1)
gen Master = (tiplau_micro==2)
gen Bachelor = (tiplau_micro==3) 

*Rewrite Degree_type so as to be divided into "STEM (Scientific, Technological, Economic, Mathematics) subjects",
*"Human Science subjects"
destring gruppo_micro, replace
gen stem_subjects = (gruppo_micro==1 |gruppo_micro==2 |gruppo_micro==3 |gruppo_micro==4 |gruppo_micro==5 |gruppo_micro==6 |gruppo_micro==7 |gruppo_micro==8)
gen Human_science_subjects = (gruppo_micro==9 |gruppo_micro==10 |gruppo_micro==11 |gruppo_micro==12 |gruppo_micro==13 |gruppo_micro==14 |gruppo_micro==15 |gruppo_micro==16)

tab stem_subjects
tab Human_science_subjects

*Rewrite Level_of_satisfaction in a dummy variable in sach a way as to simplify the model.  
destring l2_55, replace
recode l2_55 (0/5 = 0) (6/10 = 1)

*Rewrite Age so as to be divided in class. The class means how old were students when they
*graduated  
destring eta_laurea_mfr, replace
gen minor_22 = (eta_laurea_mfr==1)
gen age_22_23 = (eta_laurea_mfr==2)
gen age_25_29 = (eta_laurea_mfr==3)
gen age_sup_30 = (eta_laurea_mfr==4)

*Rewrite Foreign_State_Erasmus so as to be divided into "UE", "USA" and "rest of the word". 
*This variable indicates where students went on Erasmus
destring l1_18_mfr, replace
gen UE = (l1_18_mfr==901 |l1_18_mfr==902 |l1_18_mfr==903 |l1_18_mfr==904 |l1_18_mfr==905 |l1_18_mfr==906 |l1_18_mfr==907)
gen USA =(l1_18_mfr==908)
gen rest_of_word = (l1_18_mfr==909)
drop if l1_18_mfr==998

tab sesso
tab l1_17
tab l1_20
tab l2_1
tab tiplau_micro
tab gruppo_micro
tab l2_55
tab age_22_23
tab age_25_29
tab age_sup_30
tab minor_22



***************************************************************************************************************************

*** D) RENAME VARIABLES

rename reg_uni_micro region
rename tiplau_micro qualification
rename sesso sex
rename eta_laurea_mfr age 
rename l1_17 erasmus
rename l1_18_mfr foreign_state_eramus
rename l1_20 in_progress
rename l1_22 grade
rename l2_1 job
rename l2_55 level_satisfaction
rename gruppo_micro dregre_type

*Rewrite region so as to be divided into the 20 region of Italy
destring region, replace
drop if region==99
tab region
label values region
label define impo 1 "Piemonte"	2 "Valle_dAosta" 3 "Lombardia" 4 "Trentino_Alto_Adige" 5 "Veneto" 6 "Friuli_Venezia_Giulia" 7 "Liguria"	8 "Emilia_Romagna" 9 "Toscana" 10 "Umbria" 11 "Marche" 12 "Lazio" 13 "Abruzzo" 14 "Molise" 15 "Campania" 16 "Puglia" 17 "Basilicata" 18 "Calabria" 19 "Sicilia" 20 "Sardegna"
label values region impo
tab region



**************************************************************************************************************************

*** E) CONTEXT AND DESCRIPTION VARIABLES

***Regional Unemployment Rate variable
*Calculate how many people don't work per region.
*A high or low unemployment rate ratio in the abitation region could influence the choice of job search

gen regional_unemployment_rate= 8.43 if region==1
replace regional_unemployment_rate= 6.86 if region==2
replace regional_unemployment_rate= 6.30 if region==3
replace regional_unemployment_rate= 4.28 if region==4
replace regional_unemployment_rate= 6.18 if region==5
replace regional_unemployment_rate= 6.47 if region==6
replace regional_unemployment_rate= 11.84 if region==7
replace regional_unemployment_rate= 6.12 if region==8
replace regional_unemployment_rate= 7.72 if region==9
replace regional_unemployment_rate= 10.38 if region==10
replace regional_unemployment_rate= 9.14 if region==11
replace regional_unemployment_rate= 11.56 if region==12
replace regional_unemployment_rate= 10.73 if region==13
replace regional_unemployment_rate= 11.75 if region==14
replace regional_unemployment_rate= 21.60 if region==15
replace regional_unemployment_rate= 16.74 if region==16
replace regional_unemployment_rate= 14.38 if region==17
replace regional_unemployment_rate= 24.34 if region==18
replace regional_unemployment_rate= 22.29 if region==19
replace regional_unemployment_rate= 16.39 if region==20

label variable regional_unemployment_rate


*** Variable Number PMI (Small and medium_size enterprises variable) per region.
*The presence of many PMI could influence the probability of looking for a new job, 
*bacause PMI rarely offer career opportunities and they hire infrequently.

gen n_PMI = 101961  if region==1
replace n_PMI = 4048 if region == 2
replace n_PMI = 265383 if region == 3
replace n_PMI = 34061 if region == 4
replace n_PMI = 115876 if region == 5
replace n_PMI = 28756 if region == 6
replace n_PMI = 41493 if region == 7
replace n_PMI = 120797 if region == 8
replace n_PMI = 113312 if region == 9
replace n_PMI = 23478 if region == 10
replace n_PMI = 45154 if region == 11
replace n_PMI = 153958 if region == 12
replace n_PMI = 36392 if region == 13
replace n_PMI = 7705 if region == 14
replace n_PMI = 136297 if region == 15
replace n_PMI = 102666 if region == 16
replace n_PMI = 13388 if region == 17
replace n_PMI = 42705 if region == 18
replace n_PMI = 107460 if region == 19
replace n_PMI = 41204 if region == 20

label variable n_PMI Number_Small_Medium_size_Entreprises

*** Variabile Statuto speciale (1/0)
*Regions "a statuto speciale" enjoy particular privileges on a financial level and this could lead to greater 
*job opportunities due to regional fiscal well-being

gen statuto_speciale = 0  if region==1
replace statuto_speciale = 1 if region == 2
replace statuto_speciale = 0 if region == 3
replace statuto_speciale = 0 if region == 4
replace statuto_speciale = 0 if region == 5
replace statuto_speciale = 1 if region == 6
replace statuto_speciale = 0 if region == 7
replace statuto_speciale = 0 if region == 8
replace statuto_speciale = 1 if region == 9
replace statuto_speciale = 0 if region == 10
replace statuto_speciale = 0 if region == 11
replace statuto_speciale = 0 if region == 12
replace statuto_speciale=  0 if region == 13
replace statuto_speciale = 0 if region == 14
replace statuto_speciale = 0 if region == 15
replace statuto_speciale = 0 if region == 16
replace statuto_speciale = 0 if region == 17
replace statuto_speciale = 0 if region == 18
replace statuto_speciale = 1 if region == 19
replace statuto_speciale = 1 if region == 20

*** Population Density Variable
*A high (or low) density of inhabitants may indicate the conspicuous presence (or not) of job opportunities in that region

gen population_density= 171 if region==1
replace population_density = 38 if region == 2
replace population_density = 422 if region == 3
replace population_density = 79 if region == 4
replace population_density = 268 if region == 5
replace population_density = 153 if region == 6
replace population_density = 285 if region == 7
replace population_density = 199 if region == 8
replace population_density = 162 if region == 9
replace population_density = 104 if region == 10
replace population_density = 161 if region == 11
replace population_density = 340 if region == 12
replace population_density = 121 if region == 13
replace population_density = 68 if region == 14
replace population_density = 423 if region == 15
replace population_density = 205 if region == 16
replace population_density = 55 if region == 17
replace population_density = 126 if region == 18
replace population_density = 192 if region == 19
replace population_density = 67 if region == 20


label variable population_density Number_inhabitants_per_km2

label variable statuto_speciale Region_a_statuto_speciale

desc regional_unemployment_rate n_PMI statuto_speciale population_density

*** DESCRIPTIVE TABLES
*Description of context variables in relation to Y. 
tab regional_unemployment_rate job
tab n_PMI job
tab statuto_speciale job
tab population_density job

sum regional_unemployment_rate n_PMI population_density

***From the descriptive tables it emerges that both the variable relating to the number of PMI and the population 
*density have a rather wide and high range of values.
*To better evaluate the effects of these variables, it will be necessary to take into account the amplitude of the
*values and the respective units of measurement

***************************************************************************************************************************

*I used a Logistic Regression with Personal Variable, then two multilevel analsys follow for the abitation region, 
*where in the second analysis  were added context variables. 

*** F) LOGISTIC REGRESSION

logit job sex Bachelor Combined_degree Master erasmus stem_subjects in_progress grade level_satisfaction age_22_23 age_25_29 age_sup_30 UE USA 

*The independent personal variables are almost all statistically significant, with the exception of the Master variable.
*The Bachelor, the Combined degree and the Master are not statistically significant

*** G) MULTILEVEL
*** without context variables

melogit job sex Bachelor Combined_degree Master erasmus stem_subjects in_progress grade level_satisfaction age_22_23 age_25_29 age_sup_30 UE USA || region:
estat icc

*** RANDOM EFFECT'S CALCULATION 
predict re, remodes
sort region
by region: gen n =_n
sort re region
list region re if (n == 1)

*Looking at the results of the random effect we see that with the same characteristics, it is less probability
*that a person who lives in the Marche, Abruzzo, Liguria, Calabria will find work easily, while it is more likely 
*for those who live in Tuscany, Umbria and Sicily.

*** WITH CONTEXT VARIABLES
melogit job sex Bachelor Combined_degree Master erasmus stem_subjects in_progress grade level_satisfaction age_22_23 age_25_29 age_sup_30 UE USA region regional_unemployment_rate n_PMI statuto_speciale population_density || region:
estat icc

*Context variables have been added to the analysis.
*Let's see how the population density and the number of PMI in the region are significant.
*The unemployment rate and the statuto speciale, on the other hand, are not significant.
